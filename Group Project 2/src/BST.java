
 public class BST {
	Node root;

	private class Node {
		String keyword;
		Record record;
		int size;
		Node l;
		Node r;

		// Initializes keyword k.
		private Node(String k) {
			keyword = k;
		}
		// Adds the Record r to the linked list of records.
		private void update(Record r) {
			if (record == null) {
				record = r;
			}
			else {
				r.next = record;
				record = r;    
			}
		}
	}

	public BST()  {
		this.root = null;
	}
	// Recursive insertion that adds recordToAdd to the list of records for the node
	// associated with keyword. If there is no node, this code should add the node.
	public void insert(String keyword, FileData fd) {
		Record recordToAdd = new Record(fd.id, fd.author, fd.title, null);
		keyword = keyword.trim();
		insert(keyword, recordToAdd, root);
	}
	
	private void insert(String keyword, Record recordToAdd, Node root) {
		// If the root is null, then it creates a new node and makes it the root.
		if (root == null) {
			Node node = new Node(keyword);
			node.update(recordToAdd);
			this.root = node;
		}
		
		// Traverses BST in alphabetical order to add record.
		else if (keyword.compareTo(root.keyword) < 0) {
			if (root.l != null) {
				insert(keyword, recordToAdd, root.l);
			}
			else {
				Node node = new Node(keyword);
				node.update(recordToAdd);
				root.l = node;
			}
		} 
		else if (keyword.compareTo(root.keyword) > 0) {	
			if (root.r != null) {	 
				insert(keyword, recordToAdd, root.r);
			}
			else {
				Node node = new Node(keyword);  
				node.update(recordToAdd); 
				root.r = node; 
			}
		}
		// Updates record after.
		else {	
			root.update(recordToAdd);
		}
	}
	
	// Recursive function which returns true if a particular keyword exists in the BST.
	public boolean contains(String keyword) { 
		return contains(keyword, root);
	}
  
	private boolean contains(String keyword, Node root) {
		// Returns false if the root is null.
		if (root == null)
			return false;
		
		// Traverses BST in alphabetical order searching for the keyword.
		if (keyword.compareTo(root.keyword) < 0) {
			return contains(keyword, root.l);
		}
		else if (keyword.compareTo(root.keyword) > 0) {	 
			return contains(keyword, root.r);
		}
		// Returns true if that particular keyword exists in BST.
		else {	  
			return true;
		}
	}
 
	// Returns the first record for a particular keyword. This record will link
	// to other records. If the keyword is not in the BST, it returns null.
	public Record get_records(String keyword) {
		Node current = root;
		// This iterates the loop until current node is null.
		while (current != null) {
			// Traverses BST in alphabetical order searching for the keyword.
			if (keyword.compareTo(current.keyword) < 0) {	 
				current = current.l;
			}
			else if (keyword.compareTo(current.keyword) > 0) {
				current = current.r;
			}
			// The keyword is found, returns the first record.
			else {
				return current.record;
			}
		}
		return null;
	}
	
	// Recursive function which removes the Node with keyword from the binary search tree.
	public void delete(String keyword) {
		delete(keyword, root);
	}

	private Node delete(String keyword, Node current) {
		// Returns null if the keyword does not exist.
		if (current == null) {
			return null;
		}
		
		// Traverses BST in alphabetical order searching for the keyword.
		else if (keyword.compareTo(current.keyword) < 0) {
			current.l = delete(keyword, current.l);
		}
		else if (keyword.compareTo(current.keyword) > 0) {
			current.r = delete(keyword, current.r);
		}
		else {
			if (current.r == null) {
				current = current.l;
			}
			else {
				// The node to delete has a left and right side. This finds the
				// smallestNode value on the right subtree.
				Node replacement = smallestNode(current.r);
				// This moves the value into the node.
				current.keyword = replacement.keyword;
				current.record = replacement.record;
				current.size = replacement.size;
				// This deletes the smallestNode node.
				current.r = delete(replacement.keyword, current.r);
			}
		}
		return current;
	}

	// Recursive helper method. Returns the smallest node.
	private Node smallestNode(Node root) {
		if (root == null) {
			return null;
		}
		if (root.l == null) {
			return root;
		}
		return smallestNode(root.l);
	}

	public void print() {
		print(root);
	}

	private void print(Node t) {
		if (t != null) {
			print(t.l);
			System.out.println(t.keyword); 
			Record r = t.record;
			while (r != null) {
				System.out.printf("\t%s\n", r.title);
				r = r.next;
			}
			print(t.r);
		}
	}
}
 
 