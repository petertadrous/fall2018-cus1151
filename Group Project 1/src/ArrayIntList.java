import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class ArrayIntList {
	private int[] elementData;
	private int size;
	
	public static final int DEFAULT_CAPACITY = 100;
	
	public ArrayIntList() {
		this(DEFAULT_CAPACITY);
	}
	
	public ArrayIntList(int capacity) {
		elementData = new int[capacity];
		size = 0;
	}	
	
	public void add(int value) {
        add(size, value);
    }
	
	public void add(int index, int value) {
        if (index < 0 || index > size) {
            throw new IndexOutOfBoundsException("index: " + index);
        }
        ensureCapacity(size + 1);
        for (int i = size; i > index; i--) {
            elementData[i] = elementData[i - 1];
        }
        elementData[index] = value;
        size++;
    }
	
	public boolean contains(int value) {
        return indexOf(value) >= 0;
    }
	
	public void ensureCapacity(int capacity) {
		if (capacity > elementData.length) {
			int newCapacity = elementData.length * 2 + 1;
			if (capacity > newCapacity)
				newCapacity = capacity;
			int[] newList = new int[newCapacity];
			for (int i = 0; i < size; i++)
				newList[i] = elementData[i];
			elementData = newList;
		}
	}
	
	public int get(int index) {
		checkIndex(index);
		return elementData[index];
	}
	
	public int indexOf(int value) {
		for (int i = 0; i < size; i++) {
			if (elementData[i] == value) {
				return i;
			}
		}
		return -1;
    }
	
	public boolean isEmpty() {
        return size == 0;
    }
	
	public void remove(int index) {
        checkIndex(index);
        for (int i = index; i < size - 1; i++) {
            elementData[i] = elementData[i + 1];
        }
        size--;
    }
	
	public void set(int index, int value) {
        checkIndex(index);
        elementData[index] = value;
    }
	
	public int size() {
        return size;
    }
	
	private void checkIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("index: " + index);
        }
    }
	
	public String toString() {
        if (size == 0) {
            return "[]";
        } else {
            String result = "[" + elementData[0];
            for (int i = 1; i < size; i++) {
                result += ", " + elementData[i];
            }
            result += "]";
            return result;
        }
    }
	
	
	
	
	
	
	// Q1. Method maxCount() returns the number of occurrences of the most
	//     frequently occurring value in a sorted list of integers.
	public int maxCount() {
		// Checks if the list is empty
		if(size == 0)
			return 0;
		// If there is at least one value in the list, the longest and count are 1.
		int longest = 1;
		int count = 1;
		// Checks if each element is equal to the previous.
		for(int i = 1; i < size; i++) {
			// If the values are equal, the count increases.
			if(elementData[i] == elementData[i-1])
				count++;
			// Else, count resets to 1.
			else
				count = 1;
			// If the current count is longer than previous longest, sets longest = count.
			if(count > longest)
				longest = count;
		}
		// Returns the number of occurrences of the most frequently occurring value.
		return longest;
	}
	
	// Q2. Method longestSortedSequence() returns the length of the longest sorted
	//     sequence within a list of integers
	public int longestSortedSequence() {
		// Checks if the list is empty
		if(size == 0)
			return 0;
		// If there is at least one value in the list, the longest and count are 1.
		int longest = 1;
		int count = 1;
		// Checks if each element is equal to or greater than the previous.
		for(int i = 1; i < size; i++) {
			// If the values are in order, the count increases.
			if(elementData[i-1] <= elementData[i])
				count++;
			// Else, count resets to 1.
			else
				count = 1;
			// If the current count is longer than previous longest, sets longest = count.
			if(count > longest)
				longest = count;
		}
		// Returns the number of occurrences of the most frequently occurring value.
		return longest;
	}
	
	// Q3. Method runningTotal() returns a new ArrayIntList that contains a
	//	   running total of the original list.
	public ArrayIntList runningTotal() { 
		ArrayIntList total = new ArrayIntList(); 
		if(size > 0) {
			// Sets the first element of new list as first element of original list.
			total.elementData[0] = elementData[0]; 
			// For each element, sets the new element as the running total.
			for(int i = 1; i < size; i++) {
				total.elementData[i] = total.elementData[i-1] + elementData[i]; 
			}
			// Sets the size of new list.
			total.size = size; 
		} 
		// Returns the new list of running totals.
		return total; 
	}
	
	// Q4. Method isPairwiseSorted() returns whether or not a list of integers
	//	   is pairwise sorted.
	public boolean isPairwiseSorted() {
		// For length of list, checks if element is sorted. Then i+=2 and checks the next 2.
		for(int i = 0; i < size - 2; i += 2) {
			// If the element is greater than the next, returns false.
			if(elementData[i] > elementData[i+1]) 
				return false;
		}
		// Otherwise, returns true.
		return true;
	}
	
	// Q5. Method copyStack() takes a stack of integers as a
	//	   parameter and returns a copy of the original stack.
	public static Stack<Integer> copyStack(Stack<Integer> s) {
		// Creates new stack and auxiliary queue.
		Stack<Integer> copy = new Stack<Integer>();
	    Queue<Integer> aux = new LinkedList();
	    // While the input stack is not empty, it pops the elements into the copy stack.
	    while(!s.isEmpty()) {
	        copy.push(s.pop());
	    }
	    // While copy isn't empty, it pops elements back in s and the aux in correct order.
	    while(!copy.isEmpty()) {
	        int num = copy.pop();
	        aux.add(num);
	        s.push(num);
	    }
	    // While the aux queue is not empty, it pushes them into the copy in correct order.
	    while(!aux.isEmpty()) {
	    	copy.push(aux.remove());
	    }
	    // Returns copy of stack and original remains unchanged.
	    return copy;
	}
	
	// Q6. Method equals() takes as parameters two stacks of integers and
	//     returns true if the two stacks are equal and that returns false otherwise.
	public static boolean equals(Stack<Integer> stack1, Stack<Integer> stack2) {
		// Creates aux Stack and size counts for the two parameters, as well as a boolean.
		Stack<Integer> aux = new Stack();
	    boolean equal = true;
	    int size1 = stack1.size();
		int size2 = stack2.size();                                 
	    // If the sizes are not equal, then the stacks are obviously not equal.
		if(size1 != size2)
			return false;
		// Puts elements from both stacks in aux and checks if they're equal.
		while(!stack1.isEmpty()) {
	        int a = stack1.pop();
	        aux.push(a);
	        int b = stack2.pop();
	        aux.push(b);
	        // If at any point they are not equal, it sets bool to false and breaks the while.
	        if(a != b) {
	            equal = false;
	            break;
	        }
	    }
	    // Puts the values back in original stacks.
	    while(!aux.isEmpty()) {
	        stack2.push(aux.pop());
	        stack1.push(aux.pop());
	    }
	    // Returns if they are equal or not.
	    return equal;
	}
	
	// Q7. Method rearrange() takes a queue of integers as a parameter and rearranges
	//	   the order of the values so that all of the even values appear before the
	//	   odd values and that otherwise preserves the original order of the list. 
	public static void rearrange(Queue<Integer> q) {
		// Creates aux Stack and size counts for the parameter.
		Stack<Integer> aux = new Stack();
		int sz = q.size();
		// This outer loop runs twice for ordering purposes.
		for(int i = 0; i < 2; i++) {
			// Inner loop puts evens at end of queue and puts odds in aux stack.
			for(int j = 0; j < sz; j++) {
				if(q.peek() % 2 == 0)
					q.add(q.remove());
				else
					aux.push(q.remove());
			}
			// Puts odds back into queue.
			while(!aux.isEmpty())
				q.add(aux.pop());
		}
	}

	// Q8. Method isPalendrome() takes a queue of integers as a parameter and returns true
	//	   if the numbers in the queue represent a palindrome (and false otherwise).
	public static boolean isPalindrome(Queue<Integer> queue) {
		// Creates aux stack, size count for parameter, and boolean.
		Stack<Integer> aux = new Stack();
	    boolean pal = true;
	    int qSize = queue.size();
	    // Copies the queue to the stack, keeping the queue the same.
	    for(int i = 0; i < qSize; i++) {
			int n = queue.remove();
			queue.add(n);
			aux.push(n);
		}
	    // Compares queue value (first in first out), with stack value (first in last out).
	    for(int i = 0; i < qSize; i++) {
			int n1 = queue.remove();
			int n2 = aux.pop();
			// If not equal, then queue isn't a palindrome, but loop continues to preserve order.
			if(n1 != n2)
				pal = false;
			queue.add(n1);
		}
	    // Returns boolean value, if palindrome or not.
	    return pal;
	}
	
	// Q9. Method shift() takes a stack of integers and an integer n
	// 	   as parameters and that shifts n values from the bottom of
	// 	   the stack to the top of the stack.
	public static void shift(Stack<Integer> s, int n) {
		// Creates aux queue.
		Queue<Integer> aux = new LinkedList();
		// This position variable is necessary when dealing with both Stack and Queue.
		int position = s.size() - n;
		// Puts the whole stack into the aux queue, now in reverse order.
		while(!s.isEmpty()) 
			aux.add(s.pop());
		// Puts the values we want shifted back into the stack, still in reverse order.
		for(int i = 0; i < position; i++)
			s.push(aux.remove());
		// Puts the values we want shifted back into the aux queue, now in correct order.
		// The rest of the values are still in reverse order.
		while(!s.isEmpty()) 
			aux.add(s.pop());
		// Moves the values we want shifted to the front of the aux queue.
		for(int i = 0; i < n; i++)
			aux.add(aux.remove());
		// Puts all the values back into original stack, now with the values shifted to the top.
		while(!aux.isEmpty()) 
			s.push(aux.remove());
	}
	
}