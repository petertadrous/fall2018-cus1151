import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class ArrayIntListTest extends ArrayIntList {

	public static void main(String[] args) {
		ArrayIntList a1 = new ArrayIntList();
		Stack<Integer> s1 = new Stack();
		Queue<Integer> q1 = new LinkedList();
		Queue<Integer> q2 = new LinkedList();
		
		// Create array in ascending order with some repeats
		int[] a2 = {1,2,3,4,4,4,5,6,7,8,9,10};
		
		// Copies to a list, a stack, and two queues
		for(int i=0;i<a2.length;i++) {
			a1.add(a2[i]);
			s1.push(a2[i]);
			q1.add(a2[i]);
			q2.add(a2[i]);
		}
		
		// Q1. Checks what the max repeating amount is (should return 3)
		System.out.println(a1.maxCount());
		System.out.println();
		
		// Q2. How long the longest sorted sequence is (should return length of list)
		System.out.println(a1.longestSortedSequence());
		System.out.println();
		
		// Q3. Adds each term to the last one without changing the initial list
		System.out.println(a1.runningTotal());
		System.out.println();
		
		// Q4. Since the whole array is in sequential order, this is true
		System.out.println(a1.isPairwiseSorted());
		System.out.println();
		
		// Q5. Copies s1 to s3
		System.out.println(s1);
		Stack<Integer> s3 = copyStack(s1);
		System.out.println(s3);
		System.out.println();
		
		// Q6. Both stacks are copies, so this is also true
		System.out.println(equals(s1,s3));
		System.out.println();
		
		// Q7. This rearranges queue q with evens before odds
		System.out.println(q1);
		rearrange(q1);
		System.out.println(q1);
		System.out.println();
		
		// Q8. This checks if q is a palindrome (false), then makes it one and checks again
		System.out.println(isPalindrome(q2));
		while (!s3.isEmpty()) {
			q2.add(s3.pop());
		}
		System.out.println(isPalindrome(q2));
		for (int i=0;i<12;i++) {
			q2.remove();
		}
		System.out.println();
		
		// Q9. This shifts the elements in s1 and shows the change
		System.out.println(s1);
		shift(s1, 6);
		System.out.println(s1);
	}

}

/*  Output:
3

12

[1, 3, 6, 10, 14, 18, 23, 29, 36, 44, 53, 63]

true

[1, 2, 3, 4, 4, 4, 5, 6, 7, 8, 9, 10]
[1, 2, 3, 4, 4, 4, 5, 6, 7, 8, 9, 10]

true

[1, 2, 3, 4, 4, 4, 5, 6, 7, 8, 9, 10]
[2, 4, 4, 4, 6, 8, 10, 1, 3, 5, 7, 9]

false
true

[1, 2, 3, 4, 4, 4, 5, 6, 7, 8, 9, 10]
[5, 6, 7, 8, 9, 10, 4, 4, 4, 3, 2, 1]
*/
